# Use the latest Ubuntu base image
FROM ubuntu:16.04

ENV TERM xterm

# Let the conatiner know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

# Set the locale
RUN locale-gen en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

# Add colours to bashrc
RUN sed -i -e "s/#force_color_prompt=yes/force_color_prompt=yes/g" /root/.bashrc

# Install nginx
RUN apt-get update -qqy && \
    apt-get install -qqy software-properties-common python-software-properties && \
    add-apt-repository ppa:nginx/stable && \
    add-apt-repository ppa:ondrej/php && \
    apt-get update -qqy && \
    apt-get install -qqy software-properties-common python-software-properties && \
    apt-get install -qqy nginx \
        php7.0-fpm \
        php7.0-cli \
        php7.0-common \
        php7.0-curl \
        php7.0-json \
        php7.0-gd \
        php7.0-mcrypt \
        php7.0-mbstring \
        php7.0-odbc \
        php7.0-pgsql \
        php7.0-mysql \
        php7.0-sqlite3 \
        php7.0-xmlrpc \
        php7.0-opcache \
        php7.0-intl \
        php7.0-xml \
        php7.0-zip \
        php7.0-bz2 \
        php7.0-dev \
        php-memcached \
        python-setuptools \
        curl \
        git \
        vim \
        supervisor \
        gcc \
        make \
        autoconf \
        libc-dev \
        pkg-config \
        re2c \
        libpcre3-dev \
        zlib1g-dev \
        libmemcached-dev \
    && apt-get -q clean -y && rm -rf /var/lib/apt/lists/* && rm -f /var/cache/apt/*.bin

# tweak nginx config
RUN sed -i -e "s/worker_processes  1/worker_processes 5/" /etc/nginx/nginx.conf && \
    sed -i -e "s/keepalive_timeout\s*65/keepalive_timeout 2/" /etc/nginx/nginx.conf && \
    sed -i -e "s/keepalive_timeout 2/keepalive_timeout 2;\n\tclient_max_body_size 100m/" /etc/nginx/nginx.conf && \
    echo "daemon off;" >> /etc/nginx/nginx.conf

# tweak php-fpm config
RUN sed -i -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.0/fpm/php.ini && \
    sed -i -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 100M/g" /etc/php/7.0/fpm/php.ini && \
    sed -i -e "s/post_max_size\s*=\s*8M/post_max_size = 100M/g" /etc/php/7.0/fpm/php.ini && \
    sed -i -e "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.0/cli/php.ini && \
    sed -i -e "s/upload_max_filesize\s*=\s*2M/upload_max_filesize = 100M/g" /etc/php/7.0/cli/php.ini && \
    sed -i -e "s/post_max_size\s*=\s*8M/post_max_size = 100M/g" /etc/php/7.0/cli/php.ini && \
    sed -i -e "s/;daemonize\s*=\s*yes/daemonize = no/g" /etc/php/7.0/fpm/php-fpm.conf && \
    sed -i -e "s/;catch_workers_output\s*=\s*yes/catch_workers_output = yes/g" /etc/php/7.0/fpm/pool.d/www.conf && \
    sed -i -e "s/pm.max_children = 5/pm.max_children = 9/g" /etc/php/7.0/fpm/pool.d/www.conf && \
    sed -i -e "s/pm.start_servers = 2/pm.start_servers = 3/g" /etc/php/7.0/fpm/pool.d/www.conf && \
    sed -i -e "s/pm.min_spare_servers = 1/pm.min_spare_servers = 2/g" /etc/php/7.0/fpm/pool.d/www.conf && \
    sed -i -e "s/pm.max_spare_servers = 3/pm.max_spare_servers = 4/g" /etc/php/7.0/fpm/pool.d/www.conf && \
    sed -i -e "s/pm.max_requests = 500/pm.max_requests = 200/g" /etc/php/7.0/fpm/pool.d/www.conf

# fix ownership of sock file for php-fpm
RUN sed -i -e "s/;listen.mode = 0660/listen.mode = 0750/g" /etc/php/7.0/fpm/pool.d/www.conf && \
    find /etc/php/7.0/cli/conf.d/ -name "*.ini" -exec sed -i -re 's/^(\s*)#(.*)/\1;\2/g' {} \;

# nginx site conf
RUN rm -Rf /etc/nginx/conf.d/* && \
    rm -Rf /etc/nginx/sites-available/default && \
    mkdir -p /etc/nginx/ssl/
ADD ./site-available.conf /etc/nginx/sites-available/default.conf
RUN ln -s /etc/nginx/sites-available/default.conf /etc/nginx/sites-enabled/default.conf

# Install composer
RUN curl -sS https://getcomposer.org/installer | php && \
    mv composer.phar /usr/local/bin/composer

RUN sed -i "s/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/g" /etc/php/7.0/cli/php.ini
RUN sed -i "s/memory_limit = 128M/memory_limit = 256M /g" /etc/php/7.0/fpm/php.ini

# phalcon
# Set environment variables
ENV ZEPHIRDIR=/usr/share/zephir \
    PATH=$PATH:/usr/share/zephir/bin

COPY docker-entrypoint.sh /docker-entrypoint.sh

# Install Zephir
RUN mkdir -p /usr/share/zephir && \
    git clone --depth=1 -v https://github.com/phalcon/zephir /tmp/zephir && \
    cd /tmp/zephir && \
    echo "#!/usr/bin/env bash\nexec \"\$@\"" | tee /usr/bin/sudo && \
    chmod +x /usr/bin/sudo && \
    (cd parser && phpize --clean) && \
    ./install -c && \
    chmod +x /docker-entrypoint.sh && \
    rm -rf /tmp/*
# Set up the Zephir directory
VOLUME ["/zephir"]
# Define working directory
WORKDIR /zephir
# Set up the command arguments
CMD ["/docker-entrypoint.sh"]

# Phalcon
RUN mkdir -p /opt/
RUN cd /opt && git clone http://github.com/phalcon/cphalcon -b 2.1.x --single-branch && \
    cd /opt/cphalcon && zephir build --backend=ZendEngine3 && \
    rm -rf /opt/cphalcon && \
    echo "extension=phalcon.so" >> /etc/php/7.0/fpm/conf.d/20-phalcon.ini && \
    echo "extension=phalcon.so" >> /etc/php/7.0/cli/conf.d/20-phalcon.ini
RUN composer require "phalcon/devtools" -d /usr/local/bin/
RUN ln -s /usr/local/bin/vendor/phalcon/devtools/phalcon.php /usr/bin/phalcon

# Supervisor Config
ADD ./supervisord.conf /etc/supervisord.conf

# Start Supervisord
ADD ./start.sh /start.sh
RUN chmod 755 /start.sh

RUN usermod -u 1000 www-data && usermod -a -G users www-data && \
    mkdir -p /var/www/html && chown -R www-data:www-data /var/www && \
    mkdir /run/php && chown www-data:www-data -R /run/php
# add test PHP file
ADD ./index.php /var/www/html/index.php

WORKDIR /var/www/html